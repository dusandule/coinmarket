import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {DataService} from "../services/data.service";
import {CurrencyDetails} from "../interfaces/currency-details";
import {NgxSpinnerService} from "ngx-spinner";

@Component({
  selector: 'app-details-list',
  templateUrl: './details-list.component.html',
  styleUrls: ['./details-list.component.css']
})

export class DetailsListComponent implements OnInit {

  /**
   * Currency details
   */
  public data: CurrencyDetails;

  constructor(
    private dataService: DataService,
    private route: ActivatedRoute,
    private router: Router,
    private spinner: NgxSpinnerService
  ) {
  }

  ngOnInit() {
    this.spinner.show().then(this.getDetails.bind(this));
  }

  /**
   * Get currency details
   */
  private getDetails() {
    this.route.paramMap.subscribe(params => {
      const id = Number(params.get('id'));
      this.dataService.getCurrencyDetailsById(id)
        .subscribe(response => {
          this.data = response.data[id];
          this.spinner.hide();
        });
    })
  }

  /**
   * Go back to list page on button click
   */
  protected goToCurrencies() {
    this.router.navigate(['/list']);
  }
}
