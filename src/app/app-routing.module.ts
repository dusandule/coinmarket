import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {CurrencyListComponent} from "./currency-list/currency-list.component";
import {DetailsListComponent} from "./details-list/details-list.component";
import {AmountInputComponent} from "./currency-list/amount-input/amount-input.component";

const routes: Routes = [
  {
    path: '',
    redirectTo: '/list',
    pathMatch: 'full'
  },
  {
    path: 'list',
    component: CurrencyListComponent
  },
  {
    path: 'details/:id',
    component: DetailsListComponent
  },
  {
    path: '**',
    component: CurrencyListComponent
  }

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {
}

/**
 * Declare all components here!
 */
export const routingComponents = [
  CurrencyListComponent,
  DetailsListComponent,
  AmountInputComponent
];
