export interface CurrencyDetails {
  category: string;
  dateAdded: string;
  description: string;
  id: number
  logo: string;
  name: string;
  symbol: string;
}
