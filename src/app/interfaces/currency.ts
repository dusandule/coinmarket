export interface Currency {
  cmc_rank: number;
  id:number;
  name:string;
  symbol: string;
  price: number;
  percentChange: number;
  currentAmount?: number;
  yourCurrencyAmount: number;
}
