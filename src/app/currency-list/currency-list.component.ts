import {AfterContentInit, Component, OnDestroy, OnInit} from '@angular/core';
import {Currency} from "../interfaces/currency";
import {DataService} from "../services/data.service";
import {Router} from "@angular/router";
import {NgxSpinnerService} from "ngx-spinner";
import {interval, Observable} from "rxjs";

@Component({
  selector: 'app-currency-list',
  templateUrl: './currency-list.component.html',
  styleUrls: ['./currency-list.component.css']
})

export class CurrencyListComponent implements OnInit, OnDestroy {

  /**
   * Current page number in list
   * @type {number} currentPageNumber
   */
  protected currentPageNumber: number = 1;

  /**
   * Items per page in list
   * @type {number} itemsPerPage
   */
  protected itemsPerPage: number = 10;

  /**
   * Data list of currencies
   * @type {Currency[]} listItems
   */
  private listItems: Currency[];

  /**
   * Refresh flag
   * @type {boolean} refresh
   */
  private refresh: boolean = true;

  /**
   * Refresh time
   * @type {number} refreshTime Time represented in milliseconds
   */
  public refreshTime: number = 60 * 1000;

  constructor(
    private dataService: DataService,
    private router: Router,
    private spinner: NgxSpinnerService
  ) {
  }

  ngOnInit() {
    this.dataService.items.subscribe(data => this.listItems = data);
    this.spinner.show().then(this.getData.bind(this));
    interval(this.refreshTime).subscribe(value => {
      if (this.refresh) {
        this.spinner.show().then(this.getData.bind(this));
      }
    });
  }

  ngOnDestroy() {
    this.refresh = false;
  }

  /**
   * When amount is changed and submitted it will calculate new value and store it in local storage
   * @param data
   */
  protected calculateCurrencyAmount(data) {
    let item = this.findItemById(data.currencyId);
    item.yourCurrencyAmount = item.price * data.amount;

    let localStorageCurrencyId = item.symbol + item.id;

    this.setYouCurrencyLocalDataById(localStorageCurrencyId, {
      currentAmount: data.amount,
      yourCurrencyAmount: item.yourCurrencyAmount
    });
  }

  /**
   * Create currency object from passed data
   * @param data
   */
  private createCurrency(data) {
    let localStorageCurrencyId = data.symbol + data.id;
    let localData = this.getYouCurrencyLocalDataById(localStorageCurrencyId);
    let currency = {
      id: data.id,
      name: data.name,
      symbol: data.symbol,
      price: data.quote.USD.price,
      percentChange: data.quote.USD.percent_change_24h,
      currentAmount: localData ? localData.currentAmount : null,
      yourCurrencyAmount: localData ? localData.yourCurrencyAmount : 0
    };
    return currency;
  }

  /**
   * Open currency details page by its id number
   * @param {number} currencyId
   */
  protected currencyDetails(currencyId: number) {
    this.router.navigate(['/details/', currencyId]);
  }

  /**
   * Find currency data item by id
   * @param {number} id
   */
  private findItemById(id: number) {
    let item = this.listItems.find(function (value) {
      return value.id === id
    });
    return item;
  }

  /**
   * Fetch data from server and set it
   */
  private getData() {
    this.dataService.fetchAllData()
      .subscribe(response => {
        let data = response.data;
        if (data && data.length) {
          let results = data.map(item => {
            return this.createCurrency(item);
          });
          this.dataService.setAllData(results);
          this.spinner.hide();
        }
      });
  }

  /**
   * Get your currency amount and current amount from local storage if exists
   * @param {string} currencyId Currency id is made by joining symbol and id of specific currency (example "BTC154")
   */
  private getYouCurrencyLocalDataById(currencyId: string) {
    return JSON.parse(localStorage.getItem(currencyId));
  }

  /**
   * Get your currency amount and current amount from local storage if exists
   * @param {string} currencyId Currency id is made by joining symbol and id of specific currency (example "BTC154")
   */
  private setYouCurrencyLocalDataById(currencyId: string, data) {
    localStorage.setItem(currencyId, JSON.stringify(data));
  }

  /**
   * Method is used to check field values and help to mark with proper class names
   * @param {number} value
   */
  protected isPositiveValue(value: number): Object {
    return {
      'positive-val': value > 0,
      'negative-val': value < 0,
    };
  }
}
