import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-currency-list-amount-input',
  templateUrl: './amount-input.component.html',
  styleUrls: ['./amount-input.component.css']
})

export class AmountInputComponent {

  /**
   * Flag for submit button state
   * @type {boolean} disable
   */
  private disabled: boolean = true;

  /**
   * RegExp validator for input field
   * Valid values examples:
   * 1,000.00
   * 1.00
   * 10000
   */
  private regExpValidator: RegExp = /^([1-9]{1}[0-9]{0,2}(\,[0-9]{3})*(\.[0-9]{0,2})?|[1-9]{1}[0-9]{0,}(\.[0-9]{0,2})?|0(\.[0-9]{0,2})?|(\.[0-9]{1,2})?)$/;

  /**
   * Value of input field
   *@type {number} amount
   */
  @Input() public amount: number;

  /**
   * Every currency row has own input and they unique value is id
   * @type {number} currencyId
   */
  @Input() public currencyId: number;

  /**
   * Fired when input value is submitted
   * @type {EventEmitter} amountChanged
   */
  @Output() public amountChanged = new EventEmitter<any>();

  /**
   * Fire event that amount in input field is changed
   * Event will emmit additional arguments:
   * Input value and selected currency id
   */
  protected changeAmount() {
    this.amountChanged.emit({
      amount: this.amount,
      currencyId: this.currencyId
    });
  }

  /**
   * Input change listener used for toggling submit button
   * @param {string} value
   */
  protected onInputChange(value) {
    let disable = !value || value.trim() === '' || !this.regExpValidator.test(value);
    this.disabled = disable;
  }

  /**
   * Fire change event method if user presses enter buttons
   * @param {string} keyCode
   */
  protected keyPressed(keyCode) {
    if ((keyCode === 'Enter' || keyCode === 'NumpadEnter') && !this.disabled) {
      this.changeAmount();
    }
  }
}
