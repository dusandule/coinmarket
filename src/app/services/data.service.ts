import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Currency} from "../interfaces/currency";
import {BehaviorSubject, Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})

export class DataService {

  // TODO Move this to config

  /**
   * API param - list starting point
   */
  private start: number = 1;

  /**
   * API param - sort by
   */
  private sort: string = 'price';

  /**
   * API param - limit number of results
   */
  private limit: number = 50;

  /**
   * API param - convert currencies into
   */
  private convert: string = 'USD';

  /**
   * API param - access api key
   */
  private apiKey: string = '36b33eec-fc04-4d62-8fcc-429f845a2121';

  /**
   * API endpoints
   */
  private endpoints = {
    list: '/v1/cryptocurrency/listings/latest',
    details: '/v1/cryptocurrency/info'
  };

  /**
   * Currency list data
   */
  private data = new BehaviorSubject<Currency[]>([]);
  items = this.data.asObservable();

  constructor(private _http: HttpClient) {
  }

  /**
   * Get details about specific currency
   * @param id
   */
  public getCurrencyDetailsById(id): Observable<any> {
    let detailsUrl = this.endpoints.details + '?id=' + id + '&CMC_PRO_API_KEY=' + this.apiKey;

    return this._http.get(detailsUrl);
  }

  /**
   * Fetch data from server
   */
  public fetchAllData(): Observable<any> {
    let fetchUrl = this.endpoints.list + '?start=' + this.start + '&sort=' + this.sort + '&limit=' + this.limit + '&convert=' + this.convert + '&CMC_PRO_API_KEY=' + this.apiKey;

    return this._http.get(fetchUrl);
  }

  /**
   * Set list of currencies
   * @param data
   */
  public setAllData(data: Currency[]) {
    this.data.next(data);
  }
}
