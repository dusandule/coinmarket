# coinmarket

### How to start project?

Checkout project and in root folder of project run `npm install` command in terminal.

To prevent problem with image not loading **DISABLE adblock** plugins on your browser.

Run `npm run start:cors-disabled` command in terminal (This will prevent problems with CORS).

Application will be available at [localhost:4200](http://localhost:4200/)


